import numpy as np
import random
import os
from settings import input_signal


def recombination(dad, mom, combination=0.66, mutations=0.1, shift=3):
    # assert dad.shape == mom.shape, "len(dad) != len(mom)"
    assert len(dad) == len(mom), "len(dad) != len(mom)"
    сhild1 = []
    сhild2 = []
    copies = False
    sh = (
        -1 * shift,
        shift,
    )
    for n in range(len(dad)):
        if combination < random.random():
            copies = not copies
        if mutations > random.random():
            сhild1.append(dad[n] + random.randint(*sh))
            сhild2.append(mom[n] + random.randint(*sh))

        elif copies:
            сhild1.append(dad[n])
            сhild2.append(mom[n])
        else:
            сhild1.append(mom[n])
            сhild2.append(dad[n])
    return сhild1, сhild2


def get_genom(genom_dct):
    dad = genom_dct[0]["genom"]
    mom = genom_dct[1]["genom"]
    son, daughter = recombination(dad, mom)
    return [dad, mom, son, daughter]


def convert_from_line(genom):
    transform = {
        1786: 33,
        1354: 25,
        1138: 21,
        1084: 20,
    }
    hid = transform[len(genom)]
    b2 = genom[-4:]
    b1_beg = sum([-1 * hid, -4])
    b1_end = -4
    b1 = genom[b1_beg:b1_end]
    l2_beg = sum([-1 * hid * 4, -1 * hid, -4])
    l2_end = sum([-1 * hid, -4])
    l2 = genom[l2_beg:l2_end]
    l1_end = sum([-1 * hid * 4, -1 * hid, -4])
    l1 = genom[:l1_end]
    return np.array(l1).reshape(input_signal, hid), np.array(l2).reshape(hid, 4), np.array(b1), np.array(b2)


def convert_to_line(l1, l2, b1, b2):
    line = np.concatenate([l1.ravel(), l2.ravel(), b1.ravel(), b2.ravel()])
    return line


def load_perceptron(tablename):
    folder_name = "data"
    if not os.path.exists(folder_name):
        os.mkdir(folder_name)
    filename = os.path.join(folder_name, tablename)
    result = np.loadtxt(filename, delimiter=";")
    return result.tolist()


def save_perceptron(line, tablename):
    folder_name = "data"
    if not os.path.exists(folder_name):
        os.mkdir(folder_name)
    filename = os.path.join(folder_name, f"{tablename}.csv")
    np.savetxt(filename, line, delimiter=";")
    return filename


def get_filenames(count=2):
    brain_file_dct = dict()
    for _, _, files in os.walk("data"):
        for f in files:
            if f == "data":
                continue
            length = f.split('.')[0].split('_')[-1]
            brain_file_dct.setdefault(length, list())
            brain_file_dct[length].append(f)

    for k, v in brain_file_dct.items():
        if len(v) < count:
            continue
        return v
    return []


def sigmoid(x):
    return 1 / (1 + np.exp(-x))


def sigmoid_derivative(x):
    return x * (1 - x)


if __name__ == "__main__":
    print(get_filenames(2))
