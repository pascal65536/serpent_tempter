import save_teacher
import numpy as np
import utils
import file_json
from settings import translate_dct, input_signal


def check():
    test_input = np.array(
        [
            [
                0.5,
                0.5,
                0.5,
                0.5,
                0.5,
                0.5,
                0.5,
                0.5,
                0.5,
                0.5,
                1.0,
                0.5,
                0.5,
                0.5,
                0.5,
                0.5,
                0.5,
                0.5,
                0.5,
                0.5,
                0.5,
                0.5,
                0.5,
                0.5,
                0.0,
                0.0,
                0.0,
                0.0,
                0.5,
                0.5,
                0.5,
                0.5,
                0.5,
                0.5,
                0.5,
                0.5,
                0.5,
                0.5,
                0.5,
                0.5,
                0.5,
                0.5,
                0.5,
                0.5,
                0.5,
                0.5,
                0.5,
                0.5,
                0.5,
            ]
        ]
    )
    test_output = sigmoid(
        np.dot(sigmoid(np.dot(test_input, layer1) + bias1), layer2) + bias2
    )
    print("Test output:", test_output)


def main(hidden_size=20, learning_rate=0.0001, num_iterations=1000):
    """
    # Define network architecture
    hidden_size = 20

    # Set hyperparameters
    learning_rate = 0.0001
    num_iterations = 1000
    """
    x_lst = list()
    y_lst = list()
    for r in save_teacher.read_data():
        y_lst.append(translate_dct.get(r[0]))
        x_lst.append(list(map(float, r[1:])))

    X = np.array(x_lst)
    y = np.array(y_lst)

    # Define network architecture
    input_size = X.shape[1]
    output_size = 4

    # Initialize weights and biases randomly
    np.random.seed(1)
    layer1 = np.random.randn(input_size, hidden_size)
    layer2 = np.random.randn(hidden_size, output_size)
    bias1 = np.random.randn(1, hidden_size)
    bias2 = np.random.randn(1, output_size)


# Set hyperparameters
learning_rate = 0.001
num_iterations = 100000

# Train the network
for i in range(num_iterations):
    # Forward propagation
    l1 = sigmoid(np.dot(X, layer1) + bias1)
    l2 = sigmoid(np.dot(l1, layer2) + bias2)

    # Backward propagation
    output_error = y - l2
    l2_delta = output_error * utils.sigmoid_derivative(l2)
    layer2_gradient = np.dot(l1.T, l2_delta)
    bias2_gradient = np.sum(l2_delta, axis=0)

    l1_delta = np.dot(l2_delta, layer2.T) * utils.sigmoid_derivative(l1)
    layer1_gradient = np.dot(X.T, l1_delta)
    bias1_gradient = np.sum(l1_delta, axis=0)

    # Update weights and biases
    layer1 += learning_rate * layer1_gradient
    layer2 += learning_rate * layer2_gradient
    bias1 += learning_rate * bias1_gradient
    bias2 += learning_rate * bias2_gradient

    # Print error every 1000 iterations
    if i % 1000 == 0:
        print("Error:", np.mean(np.abs(output_error)))

# Test the network
# [0, 1, 0, 0]
test_input = np.array(
    [
        [
            0.5,
            0.5,
            0.5,
            0.5,
            0.5,
            0.5,
            0.5,
            0.5,
            0.5,
            0.5,
            1.0,
            0.5,
            0.5,
            0.5,
            0.5,
            0.5,
            0.5,
            0.5,
            0.5,
            0.5,
            0.5,
            0.5,
            0.5,
            0.5,
            0.0,
            0.0,
            0.0,
            0.0,
            0.5,
            0.5,
            0.5,
            0.5,
            0.5,
            0.5,
            0.5,
            0.5,
            0.5,
            0.5,
            0.5,
            0.5,
            0.5,
            0.5,
            0.5,
            0.5,
            0.5,
            0.5,
            0.5,
            0.5,
            0.5,
        ]
    ]
)
test_output = sigmoid(
    np.dot(sigmoid(np.dot(test_input, layer1) + bias1), layer2) + bias2
)
print("Test output:", test_output)
