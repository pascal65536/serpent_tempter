from main import Snake, Apple, transform_dot_dct
import utils
import crud
import random
import pygame as pg
from settings import (
    horizon,
    x_size,
    y_size,
    M,
    RED,
    BLACK,
    BLUE,
    WHITE,
    GRAY,
    route_dct,
    apple_count,
    burn_coords_lst,
)


def draw_style_rect(display, color, border, coords):
    pg.draw.rect(display, border, [coords[0] - 1, coords[1], coords[2], coords[3]], 1)
    pg.draw.rect(display, border, [coords[0], coords[1] - 1, coords[2], coords[3]], 1)
    pg.draw.rect(display, border, [coords[0], coords[1], coords[2] + 1, coords[3]], 1)
    pg.draw.rect(display, border, [coords[0], coords[1], coords[2], coords[3] + 1], 1)
    pg.draw.rect(display, color, coords, 0)


def draw_text_btn(display, value=10, text="Стратегия", coords=(50, 100, 100, 50)):
    font = pg.font.Font(None, 36)

    # Отрисовка надписи
    text_surface = font.render(text, True, BLACK)
    text_rect = text_surface.get_rect(center=(display.get_width() // 2, coords[1] - 25))
    display.blit(text_surface, text_rect)
    # Отрисовка первого числового поля
    pg.draw.rect(display, GRAY, coords)
    text_surface = font.render(str(value), True, BLACK)
    text_rect = text_surface.get_rect(center=(display.get_width() // 2, coords[1] + 25))
    display.blit(text_surface, text_rect)

    left = [
        (coords[0], coords[1]),
        (coords[0], coords[1] + coords[3]),
        (coords[0] - 25, ((coords[1] * 2) + coords[3]) // 2),
    ]
    right = [
        (coords[0] + coords[2], coords[1]),
        (coords[0] + coords[2], coords[1] + coords[3]),
        (coords[0] + coords[2] + 25, ((coords[1] * 2) + coords[3]) // 2),
    ]
    # Отрисовка стрелок
    pg.draw.polygon(display, BLACK, left)
    pg.draw.polygon(display, BLACK, right)
    return {"left": left, "right": right}


def draw_controls(dsp, length=0, life=0):
    dsp.fill(WHITE)
    ret_dct = dict()
    ret_dct["length"] = draw_text_btn(
        dsp, length, text="Длина", coords=(50, 100, 100, 50)
    )
    ret_dct["life"] = draw_text_btn(dsp, life, text="Жизнь", coords=(50, 200, 100, 50))
    return ret_dct


def get_click(mouse_pos, game_width, conrols_dct):
    r = (mouse_pos[0] - game_width, mouse_pos[1])
    for k, vdct in conrols_dct.items():
        for kk, vv in vdct.items():
            if (vv[2][0] < r[0] < vv[1][0]) and (vv[0][1] < r[1] < vv[1][1]):
                return (k, kk)
            if (vv[0][0] < r[0] < vv[2][0]) and (vv[0][1] < r[1] < vv[1][1]):
                return (k, kk)


def update_game(click, life, length):
    if not click:
        return life, length
    if click[0] == "life":
        if click[1] == "left":
            life -= 1
        elif click[1] == "right":
            length = 1
            life += 1
        if life < 1:
            life = 1
    elif click[0] == "length":
        if click[1] == "left":
            length -= 1
        elif click[1] == "right":
            length += 1
            life = 1
        if length < 1:
            length = 1
    return life, length


def get_dot_lst(length=1, life=1):
    genom_lst = crud.read_max_key(life, length)
    while len(genom_lst) < 2:
        for learn_filename in utils.get_filenames(2):
            genom = utils.load_perceptron(learn_filename)
            genom_lst.append({"key": 0, "genom": genom})
    dot_lst = list()
    serpentarium_lst = list()
    genom_serpent_lst = utils.get_genom(genom_lst)
    for num, genom_serpent in enumerate(genom_serpent_lst):
        direct = random.choice(list(route_dct.values()))
        serpentarium = Snake.get_snake(burn_coords_lst[num], direct, 8, genom_serpent)
        dot_lst.append(serpentarium)
        serpentarium_lst.append(serpentarium)
    dot_lst = Apple.get_apples(apple_count, dot_lst)
    for genom in genom_lst:
        print(genom["key"], end=" ")
    print()
    return dot_lst, serpentarium_lst


def main():
    # Основной игровой цикл
    window = pg.display.set_mode((window_width, window_height))
    window.fill(WHITE)

    pg.display.set_caption("Серпентарий")
    # Задаем размеры игрового поля и панели управления
    game_width = 600
    game_height = 600
    game_surface = pg.Surface((game_width, game_height))
    game_surface.fill(RED, rect=(0, 0, game_width, game_height))
    window.blit(game_surface, (0, 0))

    control_width = 200
    control_height = 600
    control_surface = pg.Surface((control_width, control_height))
    control_surface.fill(BLUE, rect=(0, 0, control_width, control_height))

    life = 1
    length = 1
    conrols_dct = draw_controls(control_surface, length=length, life=life)
    window.blit(control_surface, (game_width, 0))

    dot_lst, serpentarium_lst = get_dot_lst(length=length, life=life)

    generation = crud.read_generation() + 1
    running = True
    while running:
        if not len(serpentarium_lst):
            generation += 1
            dot_lst, serpentarium_lst = get_dot_lst(length=length, life=life)
        # Обработка событий
        for event in pg.event.get():
            if event.type == pg.QUIT:
                running = False
            elif event.type == pg.KEYDOWN:
                if event.key == pg.K_ESCAPE:
                    running = False
            elif event.type == pg.MOUSEBUTTONDOWN:
                mouse_pos = pg.mouse.get_pos()
                click = get_click(mouse_pos, game_width, conrols_dct)
                life, length = update_game(click, life, length)
                draw_controls(control_surface, length=length, life=life)
                window.blit(control_surface, (game_width, 0))

        dot_dct = transform_dot_dct(dot_lst)
        for x in range(x_size):
            for y in range(y_size):
                color = dot_dct.get((x, y), WHITE)
                draw_style_rect(game_surface, color, BLACK, [M * x, M * y, M, M])

        window.blit(game_surface, (0, 0))

        for serpent in serpentarium_lst:
            horizon_lst = serpent.get_view(serpent.head, dot_lst, horizon)
            route = serpent.get_route(horizon_lst)
            serpent.step_count += 1
            if serpent.move(route, dot_lst):
                ind_serp = serpentarium_lst.index(serpent)
                serpentarium_lst.pop(ind_serp)
                ind_serp = dot_lst.index(serpent)
                dot_lst.pop(ind_serp)
                if serpent.step_count + len(serpent.body) > 10:
                    crud.save_data(
                        serpent.step_count, len(serpent.body), serpent.brain, generation
                    )

        # Обновление экрана
        pg.display.flip()
        pg.time.wait(1)


if __name__ == "__main__":
    print(". " * 40)
    # Задаем размеры окна
    window_width = 800
    window_height = 600
    pg.init()
    main()
    # Завершение работы Pygame
    pg.quit()
