import os
import sqlite3
import hashlib
import random
from settings import genom_db


# Создание базы данных и таблицы
def create_database():
    conn = sqlite3.connect(genom_db)
    c = conn.cursor()
    c.execute(
        """CREATE TABLE IF NOT EXISTS data (
            "id" INTEGER PRIMARY KEY AUTOINCREMENT,
            "live" INTEGER NOT NULL,
            "length" INTEGER NOT NULL,
            "md5hash" TEXT NOT NULL UNIQUE,
            "genom"	TEXT NOT NULL,
            "generation" INTEGER NOT NULL);"""
    )
    conn.commit()
    conn.close()


# Сохранение данных в БД
def save_data(live, length, genom, generation):
    genom = " ".join([str(x) for x in genom])

    md5hash = hashlib.new("md5")
    md5hash.update(genom.encode())
    md5hash_str = md5hash.hexdigest()
    if not os.path.exists(genom_db):
        create_database()

    conn = sqlite3.connect(genom_db)
    c = conn.cursor()
    c = conn.cursor()
    c.execute(
        "SELECT * from data where md5hash = ?;",
        (md5hash_str,),
    )
    rows = c.fetchall()
    if len(rows) > 0:
        return

    c.execute(
        "INSERT INTO data (live, length, md5hash, genom, generation) VALUES (?, ?, ?, ?, ?)",
        (live, length, md5hash_str, genom, generation),
    )
    conn.commit()
    conn.close()


def read_max_key(k1, k2):
    ret = list()
    if not os.path.exists(genom_db):
        create_database()
        return ret

    conn = sqlite3.connect(genom_db)
    c = conn.cursor()
    c.execute(
        "SELECT *, data.live * ? + data.length * ? as keys from data order by keys desc limit 2;",
        (k1, k2),
    )
    rows = c.fetchall()

    for row in rows:
        ret.append({"key": row[6], "genom": [float(x) for x in row[4].split(" ")]})
    conn.close()
    return ret


def read_generation():
    generation = 0
    if not os.path.exists(genom_db):
        create_database()
        return generation

    conn = sqlite3.connect(genom_db)
    c = conn.cursor()
    c.execute("SELECT * from data order by generation desc limit 1;")
    rows = c.fetchall()

    for row in rows:
        generation = int(row[5])
    return generation


if __name__ == "__main__":
    print("." * 60)
    # Создание базы данных
    create_database()

    # Заполнение первых 10 значений
    for i in range(10):
        live = random.randint(1, 100)
        length = random.randint(1, 100)
        genom = [random.randint(1, 100) for _ in range(49 * 4)]
        save_data(live, length, genom, 2)

    # Чтение данных из БД
    ret = read_max_key(10, 1)
    print(ret)
