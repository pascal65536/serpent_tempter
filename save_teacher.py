import os
from settings import teacher_csv


def save_data(horizon_lst, route):
    if not os.path.exists("data"):
        os.mkdir("data")
    filename = os.path.join("data", teacher_csv)
    with open(filename, "a+", encoding="cp1251", errors="replace", newline="") as f:
        x_lst = list()
        x_lst.append(f"{route[0]}|{route[1]}")
        x_lst.extend(list(horizon_lst.values()))
        f.write(";".join([f'"{x}"' for x in x_lst]) + "\n")


def read_data():
    ret = list()
    if not os.path.exists("data"):
        os.mkdir("data")
    filename = os.path.join("data", teacher_csv)
    if not os.path.exists(filename):
        return ret
    with open(filename, "r", encoding="cp1251") as f:
        csv_str = f.read()
        for csv in csv_str.split("\n"):
            if not csv:
                continue
            ret.append([x.strip('"') for x in csv.split(";")])
    return ret


if __name__ == "__main__":
    print("." * 60)
    output = (-1, 0)
    input_data = {
        (-3, -3): 0.5,
        (-3, -2): 0.5,
        (-3, -1): 0.5,
        (-3, 0): 0.5,
        (-3, 1): 0.5,
        (-3, 2): 0.5,
        (-3, 3): 0.5,
        (-2, -3): 0.5,
        (-2, -2): 0.5,
        (-2, -1): 0.5,
        (-2, 0): 0.5,
        (-2, 1): 0.5,
        (-2, 2): 0.5,
        (-2, 3): 0.5,
        (-1, -3): 0.5,
        (-1, -2): 0.5,
        (-1, -1): 0.5,
        (-1, 0): 0.5,
        (-1, 1): 0.5,
        (-1, 2): 0.5,
        (-1, 3): 0.5,
        (0, -3): 0.5,
        (0, -2): 0.5,
        (0, -1): 0.5,
        (0, 0): 0,
        (0, 1): 0.5,
        (0, 2): 0.5,
        (0, 3): 0.5,
        (1, -3): 0.5,
        (1, -2): 0.5,
        (1, -1): 0.5,
        (1, 0): 0,
        (1, 1): 0.5,
        (1, 2): 0.5,
        (1, 3): 0.5,
        (2, -3): 0.5,
        (2, -2): 0.5,
        (2, -1): 0.5,
        (2, 0): 0,
        (2, 1): 0.5,
        (2, 2): 0.5,
        (2, 3): 0.5,
        (3, -3): 0.5,
        (3, -2): 0.5,
        (3, -1): 0.5,
        (3, 0): 0,
        (3, 1): 0.5,
        (3, 2): 0.5,
        (3, 3): 0.5,
    }
    save_data(input_data, output)

    for row in read_data():
        print(row)

