import random
import numpy as np
import utils
from settings import (
    x_size,
    y_size,
    route_dct,
    timeout,
    BLACK,
    RED,
)


def transform_dot_dct(dot_lst):
    temp_dct = dict()
    for dt in dot_lst:
        if isinstance(dt, Snake):
            for bd in dt.body:
                temp_dct.setdefault(bd, BLACK)
        else:
            temp_dct.setdefault((dt.x, dt.y), RED)
    return temp_dct


class Apple:
    def __init__(self, *args, **kwargs):
        self.x = kwargs.get("x")
        self.y = kwargs.get("y")

    @staticmethod
    def create(dot_lst):
        while True:
            x = random.randrange(0, x_size)
            y = random.randrange(0, y_size)
            dot = (x, y)
            if dot not in dot_lst:
                apple = Apple(x=x, y=y)
                return apple

    @staticmethod
    def get_apples(count_apple, dot_lst):
        temp_lst = list()
        for dt in dot_lst:
            if isinstance(dt, Snake):
                for bd in dt.body:
                    temp_lst.append(tuple(bd))
            else:
                temp_lst.append(dt)

        for _ in range(count_apple):
            apple = Apple.create(temp_lst)
            temp_lst.append((apple.x, apple.y))
            dot_lst.append(apple)
        return dot_lst

    def __repr__(self):
        body = f"[({self.x}, {self.y})]"
        return body


class Snake:
    def __init__(self, *args, **kwargs):
        self.body = list()
        self.brain = list()
        self.name = None
        self.shedule = 0
        self.timeout = kwargs.get("timeout", timeout)
        self.head = tuple()
        self.step_count = 0

    @staticmethod
    def create():
        snake = Snake()
        x = random.randrange(0, x_size)
        y = random.randrange(0, y_size)
        snake.add(x=x, y=y)
        return snake

    @staticmethod
    def get_snake(s_tpl, direct_tpl, tail, brain_lst):
        snake = Snake()
        snake.brain = brain_lst
        sx = s_tpl[0]
        sy = s_tpl[1]
        snake.head = (sx, sy)
        for _ in range(tail):
            snake.body.append((sx, sy))
            sx += 1 * direct_tpl[0]
            sy += 1 * direct_tpl[1]
        return snake

    def get_view(self, head, dot_lst, horizon=2):
        def is_horizon(head, coord):
            if abs(head[0] - coord[0]) > horizon:
                return False
            if abs(head[1] - coord[1]) > horizon:
                return False
            return True

        horizon_lst = [
            (x, y)
            for x in range(-1 * horizon, horizon + 1)
            for y in range(-1 * horizon, horizon + 1)
        ]
        horizon_lst.sort(reverse=False)
        horizon_dct = dict()
        for hl in horizon_lst:
            horizon_dct[hl] = (head[0] + hl[0], head[1] + hl[1])
            rez = 0.5
            if not (0 <= (head[0] + hl[0]) < x_size):
                rez = 0
            if not (0 <= (head[1] + hl[1]) < y_size):
                rez = 0
            horizon_dct[hl] = rez

        for dt in dot_lst:
            if isinstance(dt, Snake):
                for bd in dt.body:
                    if is_horizon(head, bd) and dt.head != bd:
                        horizon_dct[(head[0] - bd[0], head[1] - bd[1])] = 0
                if is_horizon(head, dt.head) and head == dt.head:
                    horizon_dct[(head[0] - dt.head[0], head[1] - dt.head[1])] = 0
            else:
                if is_horizon(head, (dt.x, dt.y)):
                    horizon_dct[(head[0] - dt.x, head[1] - dt.y)] = 1
        return horizon_dct

    def move(self, route, dot_lst):
        self.shedule += 1
        new_head = (route[0] + self.head[0], route[1] + self.head[1])
        if not (0 <= new_head[0] < x_size):
            return True
        if not (0 <= new_head[1] < y_size):
            return True

        for dt in dot_lst:
            if isinstance(dt, Snake):
                for bd in dt.body:
                    if new_head == bd:
                        self.body.insert(0, new_head)
                        self.head = new_head
                        return True
            else:
                if new_head == (dt.x, dt.y):
                    dot_lst.remove(dt)
                    self.body.insert(0, new_head)
                    self.head = new_head
                    self.shedule = 0
                    Apple.get_apples(1, dot_lst)
                    return False

        self.body.insert(0, new_head)
        self.head = new_head
        self.body = self.body[:-1]
        if self.timeout < self.shedule:
            return True
        return False

    def get_route(self, horizon_lst, train=False):
        """
        Получить направление
        """
        np_array = list(horizon_lst.values())
        input_signal = np.array([np_array])
        layer1, layer2, bias1, bias2 = utils.convert_from_line(self.brain)
        output_lst = utils.sigmoid(
            np.dot(utils.sigmoid(np.dot(input_signal, layer1) + bias1), layer2) + bias2
        )
        route_lst = output_lst.tolist()[0]
        route = route_dct[route_lst.index(max(route_lst))]
        if train:
            return route, output_lst
        return route

    def __repr__(self):
        body_lst = [f"({tpl[0]}, {tpl[1]})" for tpl in self.body]
        body = f"[{', '.join(body_lst)}]"
        return body


if __name__ == "__main__":
    print("." * 60)
