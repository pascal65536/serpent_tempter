import os
import json
import random
import datetime
import string


def get_code_str():
    sad = list(string.ascii_letters + string.digits)
    random.shuffle(sad)
    return "".join(sad)


def logging(bot_name, chat_id, msg, folder_name="log"):
    if not os.path.exists(folder_name):
        os.mkdir(folder_name)
    file_name = f"{bot_name}.csv"
    filename = os.path.join(folder_name, file_name)
    with open(filename, "a+", encoding="cp1251", errors="replace", newline="") as f:
        x_lst = list()
        x_lst.append(datetime.datetime.now().isoformat())
        x_lst.append(chat_id)
        x_lst.append(msg)
        f.write(";".join([f'"{x}"' for x in x_lst]) + "\n")


def load_json(folder_name, file_name):
    if not os.path.exists(folder_name):
        os.mkdir(folder_name)
    filename = os.path.join(folder_name, file_name)
    if not os.path.exists(filename):
        with open(filename, "w") as f:
            json.dump(dict(), f, ensure_ascii=True)
    with open(filename, encoding="utf-8") as f:
        load_dct = json.load(f)
    return load_dct


def save_json(folder_name, file_name, save_dct):
    if not os.path.exists(folder_name):
        os.mkdir(folder_name)
    filename = os.path.join(folder_name, file_name)
    with open(filename, "w") as f:
        json.dump(save_dct, f, ensure_ascii=False, indent=4)


def get_fake_name(length=3):
    vowel = "aeiouy"  # гласные
    consonant = "bcdfghjklmnpqrstvwxz"  # согласные
    name = ""
    j = 0
    while j < length:
        name += random.choice(consonant)
        name += random.choice(vowel)
        j += 1
    return name.capitalize()


def get_random_number(mn=100, mx=999):
    return random.randrange(mn, mx)


if __name__ == "__main__":
    print("-" * 20)
