import random
import crud
import save_teacher
import utils
import pygame as pg
from settings import (
    route_dct,
    horizon,
    x_size,
    y_size,
    M,
    BLACK,
    WHITE,
    LIGHT_GREY,
    apple_count,
)
from main import Snake, Apple, transform_dot_dct


def draw_style_rect(display, color, border, coords):
    pg.draw.rect(display, border, [coords[0] - 1, coords[1], coords[2], coords[3]], 1)
    pg.draw.rect(display, border, [coords[0], coords[1] - 1, coords[2], coords[3]], 1)
    pg.draw.rect(display, border, [coords[0], coords[1], coords[2] + 1, coords[3]], 1)
    pg.draw.rect(display, border, [coords[0], coords[1], coords[2], coords[3] + 1], 1)
    pg.draw.rect(display, color, coords, 0)


def main():
    genom_lst = crud.read_max_key(1, 1)
    while len(genom_lst) < 2:
        for learn_filename in utils.get_filenames(2):
            genom = utils.load_perceptron(learn_filename)
            genom_lst.append({"key": 0, "genom": genom})
    genom_serpent_lst = utils.get_genom(genom_lst)
    genom_serpent = genom_serpent_lst[0]

    direct = random.choice(list(route_dct.values()))
    start = (30, 30)
    serpent = Snake.get_snake(start, direct, 8, genom_serpent)

    dot_lst = list()
    dot_lst.append(serpent)
    dot_lst = Apple.get_apples(apple_count, dot_lst)

    serpentarium_lst = list()
    serpentarium_lst.append(serpent)

    show_must_go_on = True
    display.fill(LIGHT_GREY)
    route = None
    while show_must_go_on:
        for event in pg.event.get():
            if event.type == pg.QUIT:
                show_must_go_on = False
            elif event.type == pg.KEYDOWN:
                if event.key == pg.K_LEFT:
                    route = (-1, 0)
                elif event.key == pg.K_RIGHT:
                    route = (1, 0)
                elif event.key == pg.K_UP:
                    route = (0, -1)
                elif event.key == pg.K_DOWN:
                    route = (0, 1)

        dot_dct = transform_dot_dct(dot_lst)
        for y in range(y_size):
            for x in range(x_size):
                color = dot_dct.get((x, y), WHITE)
                draw_style_rect(display, color, BLACK, [M * x, M * y, M, M])

        if not route:
            pg.display.flip()
            continue

        horizon_lst = serpent.get_view(serpent.head, dot_lst, horizon)
        serpent.step_count += 1

        if serpent.move(route, dot_lst):
            ind_serp = dot_lst.index(serpent)
            dot_lst.pop(ind_serp)
            print(horizon_lst)
            show_must_go_on = False
        save_teacher.save_data(horizon_lst, route)

        pg.display.flip()
        pg.time.wait(20)


if __name__ == "__main__":
    print(". " * 40)
    x_window_size = x_size * M
    y_window_size = y_size * M
    pg.init()
    display = pg.display.set_mode((x_window_size, y_window_size))
    window_size = (0, 0, x_window_size, y_window_size)
    main()
