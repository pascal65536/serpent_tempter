genom_db = "genom.db"
teacher_db = "teacher.db"
teacher_csv = "teacher.csv"

x_size = 60
y_size = 60
M = 10

horizon = 3
input_signal = (1 + ((horizon - 0) * 2)) ** 2

timeout = 100
apple_count = 100

translate_dct = {
    "0|-1": [1, 0, 0, 0],
    "1|0": [0, 1, 0, 0],
    "-1|0": [0, 0, 1, 0],
    "0|1": [0, 0, 0, 1],
}

route_dct = {
    0: (0, -1),
    1: (1, 0),
    2: (-1, 0),
    3: (0, 1),
}

burn_coords_lst = [(10, 10), (20, 20), (30, 30), (40, 40)]

WHITE = (255, 255, 255)
BLACK = (0, 0, 0)
RED = (255, 0, 0)
GREEN = (0, 255, 0)
BLUE = (0, 0, 255)
LIGHT_GREY = (200, 200, 200)
GRAY = (128, 128, 128)
